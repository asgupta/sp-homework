import numpy as np


def gmres(A, b, x0, N, tol):
    """Function to find the solution of linear equations represented by
        A x = b

    Args:
        A   (nd-array): (n,n) where n is the dimensionality of x)
        b   (nd-array): (n,)
        x0  (nd-array): (n,) the initial guess
        N   (int): Number of iterations to be carried out
        tol (float): Value of least residual

        Method is carried out until the number of iterations are completed or the residual falls below tol

    Returns:
        (nd-array): (n,) Solution x
        (list): Intermediate values of x 
    """

    # Ensuring dimensional congruency
    b = b.ravel()
    x0 = x0.ravel()

    # Normalising
    b_norm = np.sqrt(np.einsum('i,i->', b, b))
    A = A/b_norm
    b = b/b_norm

    # Residual
    r = b - np.einsum('ij,j->i', A, x0)

    # Initialise Q and H for later use in Arnoldi method
    Q = np.zeros((A.shape[0], N+1))
    H = np.zeros((N+1, N))

    r_norm = np.sqrt(np.einsum('i,i->', r, r))
    Q[:, 0] = (r/r_norm).ravel()
    
    beta = r_norm
    e1 = np.zeros((N+1, 1))
    e1[0] = 1

    x = x0

    # Intermediate x values
    progress_tracker=[x0]

    # Finding the solution for Ax = b
    for k in range(1, N+1):
        # Update Q and H in accordance with Arnoldi method
        Q[:, k] = np.einsum('ij,j->i', A, Q[:, k-1])
        for j in range(k):
            H[j, k-1] = np.einsum('i,i->', Q[:, j], Q[:, k])
            Q[:, k] -= (
                np.einsum('i,->i', Q[:, j], H[j, k-1])
                ).ravel()
        H[k, k-1] = np.sqrt(np.einsum('i,i->', Q[:, k], Q[:, k]))
        Q[:, k] = (
            np.einsum('i,->i', Q[:, k], 1/H[k, k-1])
            ).ravel()

        # Find y to minimise residual
        y = np.linalg.inv(
            np.einsum('ji,jk->ik', H[:k+1, :k], H[:k+1, :k])
            )
        y = np.einsum('ij,kj->ik', y, H[:k+1, :k])
        y = np.einsum('ij,jk->ik', y, e1[:k+1, :]) * beta
        
        # Update the solution
        x = x0 + np.einsum('ij,jk->ik', Q[:, :k], y).ravel()

        # Updating progress
        progress_tracker += [x]

        # Check for completion
        if np.linalg.norm(b - A @ x) < tol:
            return x, progress_tracker

    return x, progress_tracker
