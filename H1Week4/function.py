"""[N dimension Quadratic Functor functions for minimizer]
"""

import numpy as np


class QuadraticNd:
    def __init__(self, a, b) -> None:
        """Quadratic function in N dimension

        Args:
            a (Numpy matrix): Shape (N, N) where N is the no of dimension
            b (Numpy matrix): Shape (N, 1) where N is the no of dimension
        """
        
        # @todo add assertions here
        self.a = np.asarray(a)
        self.b = np.asarray(b).reshape((-1,1))
    
    def __call__(self, x):
        """Return value of quadratic function

        Args:
            x (Numpy): [Shape (N,1)]

        Returns:
            Numpy: Value of the function
        """
        
        # @todo add assert to make sure input is Nx1 
        return x.T@self.a@x/2 - x.T@self.b
