"""Run main.py to evaluate the different methods and plot the results
"""
import numpy as np
import optimizer as op
import argparse
import function

def main():

    parser = argparse.ArgumentParser(description='Program for HW1')
    
    parser.add_argument('-A','--Amatrix',nargs='+',type=float, required=True,
        help="A matrix as flattened values seprated by String. Eg A=Numpy([[1,3],[5,6]] '-A 1 3 5 6'")
    parser.add_argument('-b','--bVector', nargs='+', type=float, required=True,
        help="b vector as space separated value. Eg b=Numpy([1,2]) as '-b 1 2'")
    parser.add_argument('-x0','--xStart', nargs='+', type=float, required=True,
        help="Initial guess x0 as vector as space separated value. Eg x0=Numpy([1,2]) as '-x0 1 2'")
    parser.add_argument('-m','--methods', nargs='+', type=str, required=False,
        help="List of desired optimization methods as space seprated values. Eg '-m minimze lgmres grmes")
    parser.add_argument('-tol','--tolerance', type=float, required=False,
        help="Desired error tolerance. Eg '-tol 1e-5")
    parser.add_argument('-ub','--upperBound', nargs='+', type=float, required=False,
        help="Upperbound for each element in vector. Eg for 2D vector x, '-ub 10 8' ")
    parser.add_argument('-lb','--lowerBound', nargs='+', type=float, required=False,
        help="Lowerbound for each element in vector. Eg for 2D vector x, '-ub -10 -8' ")
    parser.add_argument('-mIter','--maxIter', type=int, required=False,
        help="Max number of iterations that optimization should run for. Eg '-maxIter 1000")
        
    args = vars(parser.parse_args())

    x0 = np.array(args['xStart'])
    dim = x0.shape[0] 
    b = np.array(args['bVector']).reshape((dim,-1))
    A = np.array(args['Amatrix']).reshape((dim,-1))

    ub = args['upperBound']
    lb = args['lowerBound']
    
    if ub != None:
        ub = np.array(ub)
    if lb != None:
        lb = np.array(lb)

    tol = args['tolerance']
    m = args['methods']
    mIter = args['maxIter']
    
    f = function.QuadraticNd(a=A, b=b)
    M = op.Minimizer(x0=x0, methods=m, upperBound=ub, lowerBound=lb, max_iteration=mIter, tol=tol)
    result = M.findMinima(fun=f)

    if m == None:
        print("Results using minimize, lgmres and gmres respectively: ", result)
    else:
        print("Results using " + ' and '.join(method for method in m) + " respectively:", result)

    M.makePlots(fun=f)

    return


if __name__ == '__main__':
    main()