"""Optimiser function - returns the minimiser
"""


import numpy as np
from scipy.optimize import minimize
from scipy.optimize import Bounds
from scipy.sparse.linalg import lgmres
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
# from mpl_toolkits import mplot3d

import GMRES


class Minimizer():
    def __init__(self, x0, methods=None, upperBound=None, lowerBound=None, max_iteration=None, tol=None) -> None:
        """[summary]

        Args:
            x0              ([ndarray], (n,))       : [initial guess]
            methods         ([list[str]], optional) : [method(s) to use]. Defaults to None.
            upperBound      ([ndarray], (n,))       : [upper bound on optimisation]. Defaults to None.
            lowerBound      ([ndarray], (n,))       : [lower bound on optimisation]. Defaults to None.
            max_iteration   ([int])                 : [max number of iterations during optimisation]. Defaults to None.
            tol             ([float])               : [tolerance]. Defaults to None.
        """
        self.methods = methods

        self.x0 = x0
        self.dim = x0.shape[0]
        
        self.max_iteration = 10000 if max_iteration==None else max_iteration        
        self.tol = 1e-8 if tol== None else tol
        self.methods = ["minimize", "lgmres", "gmres"] if  methods==None else methods

        # intermediate x values during minimisation process
        self.progress_minimize = []
        self.progress_lgmres = []
        self.progress_gmres = []

        # Minimas corresponding to the two methods
        self.minimas = []

        # Bounds for the optimisation (Optional)
        self.upperBound = upperBound 
        self.lowerBound = lowerBound

        if not self.upperBound is None and not self.lowerBound is None:
            self.bounds = Bounds(self.lowerBound, self.upperBound)
        else:
            self.bounds = None


    def callback(self, method, xi):
        """callback function to store intermediate values of x

        This function is called by the optimizer "minimize" & 
        "lgmres" at each step.

        Args:
            method  (string)            : method with which callback is executed
            xi      ([ndarray], (n,))   : intermediate values
        """
        if method == "minimize":
            self.progress_minimize += [xi]
        elif method == "lgmres":
            self.progress_lgmres += [xi]

    # Finding the minima using the specified method(s)
    def findMinima(self, fun, minimizeMethod=None):
        """Calls all the optimizer methods speficied by user

        Calls the corresponding optimizer for each method in the 
        list of methods sepecified by the user during object creation.
        This method also stores them as numpy arrays for plotting 
        purpose.  

        Args:
            fun             (functor)         : target function
            minimizeMethod  ([str], optional) : minimize method type

        Returns:
            [list]: minimizer computed by different methods
        """
        self.minimas = []
        self.progress_minimize = [self.x0]
        self.progress_lgmres = [self.x0]

        for method in self.methods:
            # Using scipy.optimize.minimize
            if method == "minimize":
                res = minimize(
                    fun=fun,
                    x0=self.x0,
                    method=minimizeMethod,
                    bounds=self.bounds,
                    tol=self.tol,
                    options={'maxiter': self.max_iteration},
                    callback= lambda xk: self.callback(method, xk)
                    ).x

            # Using scipy.sparse.linalg.lgmres
            elif method == "lgmres":
                res, info = lgmres(
                    A=fun.a,
                    b=fun.b,
                    x0=self.x0,
                    tol=self.tol,
                    maxiter=self.max_iteration,
                    callback= lambda x: self.callback(method, x)
                    )
            
            elif method == "gmres":
                res, self.progress_gmres = GMRES.gmres(
                    A=fun.a,
                    b=fun.b,
                    x0=self.x0,
                    tol=self.tol,
                    N=self.max_iteration)
            
            self.minimas += [res]
    
        self.progress_minimize = np.array(self.progress_minimize)
        self.progress_lgmres = np.array(self.progress_lgmres)
        self.progress_gmres = np.array(self.progress_gmres)
        
        return self.minimas

    def makePlots(self, fun):
        """Make plots for optimisation carried out via different methods

        Args:
            fun (functor): target function
        """

        if self.dim > 2:
            print("Plotting not available for variables with more than 2 dimensions")
            return
        
        fig = plt.figure()
        fig.suptitle('Horizontally stacked subplots')
        axes = []
        for i in range(1,len(self.methods)+1):
            axes += [fig.add_subplot(1, len(self.methods), i, projection='3d')]

        self.plotFunctionSurface(fun, axes)

        for i in range(len(self.methods)):        
            self.plotOptimizationPath(fun=fun, ax=axes[i], method=self.methods[i])
        
        plt.show()
    
        return

    def plotFunctionSurface(self, fun, axes, res_num=10):
        """Plot the surfaces for given functions on all axes

        Args:
            fun (functor): target function
            axes (list): matplotlib axis
            res_num (int, optional): Resolution for meshgrid. Defaults to 10.
        """

        # Determining the bounds on the plot
        lowerBoundPlot_0 = min(
            np.min(self.progress_minimize[:, 0]),
            np.min(self.progress_lgmres[:, 0])
            ) if self.lowerBound is None else self.lowerBound[0]
        
        upperBoundPlot_0 = max(
            np.max(self.progress_minimize[:, 0]),
            np.max(self.progress_lgmres[:, 0])
            ) if self.upperBound is None else self.upperBound[0]

        lowerBoundPlot_1 = min(
            np.min(self.progress_minimize[:, 1]),
            np.min(self.progress_lgmres[:, 1])
            ) if self.lowerBound is None else self.lowerBound[1]
        
        upperBoundPlot_1 = max(
            np.max(self.progress_minimize[:, 1]),
            np.max(self.progress_lgmres[:, 1])
            ) if self.upperBound is None else self.upperBound[1]

        lin_0 = np.linspace(start=lowerBoundPlot_0, stop=upperBoundPlot_0, num=res_num)
        lin_1 = np.linspace(start=lowerBoundPlot_1, stop=upperBoundPlot_1, num=res_num)

        grid_0, grid_1 = np.meshgrid(lin_0, lin_1)
        X_elem_0 = np.ravel(grid_0)
        X_elem_1 = np.ravel(grid_1)

        inp = np.vstack((X_elem_0, X_elem_1)).T
        Zs  = np.array([fun(x) for x in inp])
        Z   = Zs.reshape(grid_0.shape)

        # @todo set the  visual properties 
        for ax in axes:
            # ax.plot_surface(grid_0, grid_1, Z)
            ax.contour3D(grid_0, grid_1, Z, 50, cmap='binary')
            ax.set_xlabel('X[0] Label')
            ax.set_ylabel('X[1] Label')
            ax.set_zlabel('Z Label')
        
        return

    def plotOptimizationPath(self, fun, ax, method):
        """Plot the path(trace) of the optimization at each step

        Args:
            fun (functor): target function
            ax (matplotlib axis): axes of the subplot
            method (method): method for which result are plotted

        Raises:
            ValueError: [description]
        """
        if method == 'minimize':
            x = self.progress_minimize
        elif method == 'lgmres':
            x = self.progress_lgmres
        elif method == 'gmres':
            x = self.progress_gmres
        else:
            raise ValueError("Invalid method value provided")
        Z = np.array([fun(elem) for elem in x])
        # @todo set the  visual properties
        ax.plot(x[:, 0], x[:, 1], Z[:, 0], marker='o')
        ax.set_title(method)

        return
