"""Run optimization without argparse
"""
import numpy as np
import optimizer as op
import function


def main():

    # Required arguments
    x0=np.array([-2,5,1]) 
    dim = x0.shape[0] 
    A=np.array([[8,1,3], [1, 3, 2], [9, 2, 2]]).reshape((dim,-1))
    b=np.array([2, 4, 8])

    # Optional arguments - Can be substituted with None 
    methods=["minimize", "lgmres", "gmres"] 
    upperBound=np.array([5,5,5])
    lowerBound=np.array([-5, -5, -5])
    tol = 1e-8
    mIter = 10000

    # Function calls to retrieve solution and plot results
    f = function.QuadraticNd(a=A, b=b)
    M = op.Minimizer(x0=x0, methods=methods, upperBound=upperBound, lowerBound=lowerBound)
    result = M.findMinima(fun=f)

    if methods == None:
        print("Results using minimize, lgmres and gmres respectively: ", result)
    else:
        print("Results using " + ' and '.join(method for method in methods) + " respectively:", result)

    M.makePlots(fun=f)
    return


if __name__ == '__main__':
    main()
