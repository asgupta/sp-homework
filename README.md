# Homeworks 

Group: Sthithpragya Gupta & Astha Gupta

Homework directory: 
- [ ] [H1Week4](./H1Week4)
- [ ] [H2.....]()
- [ ] [H3.....]()
- [ ] [H4.....]()

Other resources 
- [notes](./notes)
    - [functor](./notes/functor.md)


## Homework for week 4. Topics: Numpy, Scipy and Matplotlib

The program has been written in a way to allow the user to use any of the optimisation methods (minimize, lgmres, gmres) for getting the solution. The 'minimize' and 'lgmres' methods are used directly from the scipy library, whereas the 'gmres' method is a custom implementation (utilising einsum). The plot making utility has also been implemented in conjunction to this.

### The package (H1Week4) contains the following files:
- function.py containing the functor
- optimizer.py containing the minimisation and plotting routines
- GMRES.py containing the GMRES method implementation using einsum
- main.py which executes the minimisation and plotting routines asking the user for arguments from the command line using argparse 
- test.py which executes the minimisation and plotting routines where arguments have to be specified within the file. This has simply been provided for quick and easy testing in conjunction with main.py.   

### Running the program using main.py
In the terminal execute the following command
```
python3 main.py -h
```
Doing so will display information about all the possible arguments as well as how to use them. A sample usecase can be:
```
python3 main.py -A 8 1 2 3 -b 1 2 -x0 3 3 -ub 10 10 -lb -5 -5
```

### Running the program using test.py
In the terminal execute the following command
```
python3 test.py
```
All the arguments have to be manually set from inside the file.
