#include "computeArithmetic.hh"

ComputeArithmetic::ComputeArithmetic(){}

double ComputeArithmetic::compute(unsigned int N){

    unsigned int sum = 0; 

    for(int i = 1; i <= N; i++){
        sum += i;
    }

    return sum;
} 

