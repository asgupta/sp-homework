#ifndef COMPUTEARITHMETIC_HH
#define COMPUTEARITHMETIC_HH

#include "series.hh"

class ComputeArithmetic : public Series {
    public:
        ComputeArithmetic();

    public:
        // double currentValue;
        double compute(unsigned int N);
};

#endif

