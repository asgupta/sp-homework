#include "computePi.hh"
#include <cmath>

ComputePi::ComputePi(){}

double ComputePi::compute(unsigned int N){

    double sum = 0.0; 

    for(int i = 1; i <= N; i++){
        sum += 1.0/std::pow(i, 2.0);
    }

    sum = std::sqrt(6.0*sum);

    return sum;
} 

double ComputePi::getAnalyticPrediction(){
    return M_PI;
}
