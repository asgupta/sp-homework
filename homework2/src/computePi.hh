#ifndef COMPUTEPI_HH
#define COMPUTEPI_HH

#include "series.hh"

class ComputePi : public Series {
    public:
        ComputePi();

    public:
        double compute(unsigned int N);
        double getAnalyticPrediction();
};

#endif

