
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <vector>
#include "computeArithmetic.hh"
#include "printSeries.hh"
#include "computePi.hh"
#include "series.hh"
#include "writeSeries.hh"



int main(int argc, char* argv []) {

    // // convert char* to stringstream
    // std::stringstream ss;
    // for(int i = 1; i < argc; i++){
    //     ss << argv[i] << ",";
    // }

    // // convert string stram to string 
    // std::cout << ss.str() << std::endl;
    // std::string s_args = ss.str();
     

    // // convert contactenated string to a vector of args
    // int start = 0; 
    // int end;
    // std::string arg_elem;
    // std::vector<std::string> args;
    // while ((end = s_args.find (',', start)) != string::npos) {
    //     arg_elem = s_args.substr (start, end - start);
    //     start = end + 1;
    //     args.push_back (arg_elem);
    // }
    // args.push_back (s_args.substr (start));

    // bool print, write, choice;

    // for(int i =0; i < argc/2; i++){
    //     if(args[i] == '-series'){
    //         if(args[i+1] == 'pi'){
    //             choice = 1;
    //         }
    //         else{
    //             std::cout << "default value arithmatic used " << std::endl;
    //             choice = 0;
    //         }
    //     }
    //     if(args[i] == '-print'){
    //         if(args[i+1] == 'no'){
    //             print = false
    //         }
    //         else{
    //             print = true
    //         }
    //     }
    //     if(args[i] == '-write'){
    //         if(args[i+1] == 'no'){
    //             write = false
    //         }
    //         else{
    //             write = true
    //         }
    //     }
    //     if(arg[i] == '')
    // }




    int choice;
    std::cout<< "Enter choice of method: Enter 0 for ComputeArithmetic or 1 for ComputePi"<<std::endl;
    std::cin >> choice;
    
    int maxitr;
    std::cout<< "Enter valeu for maxitr"<<std::endl;
    std::cin >> maxitr;

    int freq;
    std::cout<< "Enter freq"<<std::endl;
    std::cin >> freq;

    int print;
    std::cout<< "Enter 1 for print or 0 for no print "<<std::endl;
    std::cin >> print;
    int write;
    std::cout<< "Enter 1 for write or 0 for no write "<<std::endl;
    std::cin >> write;

    char delimiter;
    if(write == 1){
        std::cout<< "Enter delimiter for write "<<std::endl;
        std::cin >> delimiter;
    }


    if(choice == 0){
        ComputeArithmetic obj;
        Series & seriesPointer = obj;
        if(print == 1){
            PrintSeries print_series(seriesPointer, maxitr, freq);
            print_series.dump();
        }
        if(write == 1){
            WriteSeries write_Series(seriesPointer, maxitr, freq);
            write_Series.setSeparator(delimiter);
            write_Series.dump();
        }

    }
    else if(choice == 1){
        ComputePi obj;
        Series & seriesPointer = obj;
        if(print == 1){
            PrintSeries print_series(seriesPointer, maxitr, freq);
            print_series.dump();
        }
        if(write == 1){
            WriteSeries write_Series(seriesPointer, maxitr, freq);
            write_Series.setSeparator(delimiter);
            write_Series.dump();
        }
    }
    else{
        throw std::invalid_argument( "received unrecogised choice" );

    }

}