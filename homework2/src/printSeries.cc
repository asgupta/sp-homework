#include <iostream>
#include <cmath>
#include "printSeries.hh"
#include "series.hh"

PrintSeries::PrintSeries(
    Series& series,
    unsigned int maxiter,
    unsigned int freq): DumperSeries(series){
    // this->series = series;
    this->maxiter = maxiter;
    this->freq = freq;
    // this->series = series;

}

void PrintSeries::dump(){


    for(unsigned int i=1; i < this->maxiter/this->freq; i++){
        std::cout << this->series.compute(i*this->freq) << std::endl;
    }

    double analytical = this->series.getAnalyticPrediction();
    double numerical  = this->series.compute(this->maxiter);
    if ( std::isnan(analytical)){
        std::cout << "No Analytical Solution found" << std::endl;
    }
    else{
        std::cout << "Analytical Solution: " << analytical << std::endl;
        std::cout << "Numerical Solution: " <<  numerical << std::endl;
        std::cout << "Convergence error: " <<  analytical - numerical << std::endl;

    }

    return;
}