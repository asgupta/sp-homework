#ifndef PRINTSERIES_HH
#define PRINTSERIES_HH

#include "dumperSeries.hh"
#include "series.hh"


class PrintSeries : DumperSeries{

    public:
        unsigned int maxiter;
        unsigned int freq;

    public:
        PrintSeries(
            Series& series,
            unsigned int maxiter,
            unsigned int freq
            );

        //virtual ~PrintSeries();

    public:
        void dump();
};

#endif
