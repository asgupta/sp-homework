#include <iostream>
#include "writeSeries.hh"
#include "series.hh"
#include <fstream>
#include <sstream>
#include <string>
#include <cstring>
#include <cstdlib>

WriteSeries::WriteSeries(
    Series& series,
    unsigned int maxiter,
    unsigned int freq): DumperSeries(series){
    // this->series = series;
    this->maxiter = maxiter;
    this->freq = freq;
    // this->series = series;

}

void WriteSeries::setSeparator(char c){
    this->separator = c;
}


void WriteSeries::dump(){

    std::string fileName = "convergenceData";
    
    char sepWriter;

    if(this->separator == ','){
        fileName += ".csv";
        sepWriter = ',';
    }
    else if(this->separator == ' ' || this->separator == '\t' || this->separator == '?'){
        fileName += ".txt";
        sepWriter = ' ';
    }
    else if(this->separator == '|'){
        fileName += ".psv";
        sepWriter = '|';
    }
    else{
        std::cout << "Incorrect separator specified" << std::endl;
    }

    std::ofstream myfile;

    // Converting file name from string to char array
    std::string temp = "cat";
    char * fileNameChar = new char [fileName.length()+1];
    strcpy (fileNameChar, fileName.c_str());

    myfile.open (fileNameChar);


    // Storing data from the iterations
    double initValue = 0;
    double newValue = 0;
    double err = 0;
    double correct =  this->series.getAnalyticPrediction();
    double convergenceDist = 0;

    myfile << "calculated" << sepWriter << "error" << sepWriter << "convergenceDist" <<"\n";
    
    for(unsigned int i=1; i < this->maxiter/this->freq; i++){
        newValue = this->series.compute(i);
        err = std::abs(newValue - err);
        initValue = newValue;
        convergenceDist = std::abs(correct - newValue);

        myfile << newValue << sepWriter << err << sepWriter << convergenceDist <<"\n";
    }

    myfile.close();
    return;
}