#ifndef WRITESERIES_HH
#define WRITESERIES_HH

#include "dumperSeries.hh"
#include "series.hh"


class WriteSeries : DumperSeries{

    public:
        unsigned int maxiter;
        unsigned int freq;

    public:
        WriteSeries(
            Series& series,
            unsigned int maxiter,
            unsigned int freq
            );

        //virtual ~PrintSeries();

    protected:
        char separator = '?';

    public:
        void dump();
        void setSeparator(char c);
    
    
};

#endif
