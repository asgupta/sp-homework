#include "compute_temperature.hh"
#include "fft.hh"
// #include "material_point.hh"
#include "material_points_factory.hh"
#include <cmath>

/* -------------------------------------------------------------------------- */
void ComputeTemperature::boundaryCheck(Matrix<complex>& theta){
/*!
 * @brief set the boundary temperature to 0
 * @param[in] Matrix<complex> particle temperature (theta)
 * @param[out] Void
 *
 */
    UInt bound[2];
    bound[0] = 0;
    bound[1] = this->gridSize-1;

    // Ensuring boundary condition on temperature
    for(int rowBoundIndex = 0; rowBoundIndex < 2; ++rowBoundIndex){
        for(int col = 0; col < this->gridSize; ++col){
            theta(bound[rowBoundIndex], col) = std::complex<double>(0);
        }
    }

    for(int colBoundIndex = 0; colBoundIndex < 2; ++colBoundIndex){
        for(int row = 0; row < this->gridSize; ++row){
            theta(row, bound[colBoundIndex]) = std::complex<double>(0);
        }
    }
}


void ComputeTemperature::compute(System& system) {
/*!
 * @brief compute temperature changes in the system for one time step
 * @param[in] System: Object of class System
 * @param[out] Void
 *
 */
    /// If the boundary condition is remaining to be checked
    /// - Find the grid size, only executed once for simulation
    if(this->toCheckBoundary){
        UInt size = system.getNbParticles();
        // Assuming that the particle count is always of the form n^2
        UInt gridSize = std::sqrt(size);
        this->gridSize = gridSize;
    }

    Matrix<complex> theta(this->gridSize); // stores the temperature (theta)
    Matrix<complex> hv(this->gridSize); // stores the volumetric heat source effect

    /// Create 2D matrix of temperature values of particles
    for (auto&& entry : index(theta)) {
        int i = std::get<0>(entry);
        int j = std::get<1>(entry);
        auto& val = std::get<2>(entry);
        val = std::complex<double>(static_cast<MaterialPoint&>(system.getParticle(j*gridSize + i)).getTemperature());
    }

    /// hv: get the values for volumetric heat source
    /// and fill the material_point heat_rate attribute if required
    for (auto&& entry : index(hv)) {
        int i = std::get<0>(entry);
        int j = std::get<1>(entry);
        auto& val = std::get<2>(entry);
        auto pos = system.getParticle(j*this->gridSize + i).getPosition();
        if(this->toCheckBoundary){
            val = std::complex<double>(MaterialPointsFactory::volHeatSrcFunc(pos[0], pos[1]));
            static_cast<MaterialPoint&>(system.getParticle(j*this->gridSize + i)).getHeatRate() = val.real();
        }
        else{
            val = static_cast<MaterialPoint&>(system.getParticle(j*this->gridSize + i)).getHeatRate();
        }
    }

    /// theta: Making a boundary check, executed only once
    if(this->toCheckBoundary){
        this->boundaryCheck(theta);
        this->toCheckBoundary = false;
    }


    /// get FFTs for theta and hv
    Matrix<complex> thetaHat = FFT::transform(theta);
    Matrix<complex> hvHat = FFT::transform(hv);

    /// Calculate  wave Number Matrix
    Matrix<complex> waveNumMatrix(this->gridSize);
    /// - Save qx values in real and qy values in imaginary part
    Matrix<std::complex<int>> waveNumbers = FFT::computeFrequencies(this->gridSize);
    /// - calculate and store the qx^2 + qy^2 for each point
    for (auto&& entry : index(waveNumMatrix)) {
        int i = std::get<0>(entry);
        int j = std::get<1>(entry);
        auto& val = std::get<2>(entry);
        std::complex<int> mag = waveNumbers(i, j);
        val = std::pow(mag.real(), 2) + std::pow(mag.imag(), 2);
    }

    // Fourier transform of d(theta)/dt
    Matrix<complex> dThetaT_hat(this->gridSize);
    /// Get all constants for heat equation
    auto rho = MaterialPointsFactory::getRho();
    auto hC = MaterialPointsFactory::gethC();
    auto hK = MaterialPointsFactory::gethK();
    auto dT = MaterialPointsFactory::getdT();

    /// Computes d(theta_hat)/dt
    for (auto&& entry : index(dThetaT_hat)) {
        int i = std::get<0>(entry);
        int j = std::get<1>(entry);
        auto& val = std::get<2>(entry);
        val = (1/(rho*hC))*(hvHat(i,j) - hK*thetaHat(i, j)*(waveNumMatrix(i,j)));
    }

    /// Computes d(theta)/dt by inverse fourier transform of dThetaT_hat
    Matrix<complex> dThetaT = FFT::itransform(dThetaT_hat);

    /// Computes the new temperature distribution after dT duration
    /// theta(t+dt) = theta(t) + d(theta)/dt * dT 
    for(int row = 1; row < this->gridSize-1; ++row){
        for(int col = 1; col < this->gridSize-1; ++col){
            theta(row, col) += dThetaT(row, col)*dT;
        }
    }
    /// Ensure boundary condition on temperature
    this->boundaryCheck(theta);

    /// Update system temperature
    for(int row = 0; row < this->gridSize; ++row){
        for(int col = 0; col < this->gridSize; ++col){
            static_cast<MaterialPoint&>(system.getParticle(col*this->gridSize + row)).getTemperature() = theta(row, col).real();
        }
    }
}

/* -------------------------------------------------------------------------- */
