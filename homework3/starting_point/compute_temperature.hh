#ifndef __COMPUTE_TEMPERATURE__HH__
#define __COMPUTE_TEMPERATURE__HH__

/* -------------------------------------------------------------------------- */
#include "compute.hh"
#include "matrix.hh"

//! Compute contact interaction between ping-pong balls
class ComputeTemperature : public Compute {

  // Virtual implementation
public:
  //! Penalty contact implementation
  void compute(System& system) override;
  //! Boundary temperature reset to 0
  void boundaryCheck(Matrix<complex>& theta);

protected:
  //! To check for 0 temperature at the boundary particles at the beginning of 'compute' call
  bool toCheckBoundary = true;
  //! Stores the size of the grid (stores 'n' for a system with 'n x n' particles)
  UInt gridSize;

};

/* -------------------------------------------------------------------------- */
#endif  //__COMPUTE_TEMPERATURE__HH__
