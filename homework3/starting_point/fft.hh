#ifndef FFT_HH
#define FFT_HH
/* ------------------------------------------------------ */
#include "matrix.hh"
#include "my_types.hh"
#include <fftw3.h>
/* ------------------------------------------------------ */

/*!
 * @struct FFT
 * @brief FFTW3 wrapper functions to help transform data from Matrix class
 * to data used by the library(fftw_complex)
 */
struct FFT {
  static Matrix<complex> transform(Matrix<complex>& m);
  static Matrix<complex> itransform(Matrix<complex>& m);
  static Matrix<std::complex<int>> computeFrequencies(int size);
  static int computeFreqHelper(int size, int queryIndex);
};

/* ------------------------------------------------------ */

inline Matrix<complex> FFT::transform(Matrix<complex>& m_in){
/*!
 * @brief Wrapper to compute forward fourier transform 2D of
 * matrix<comple> numbers. Note the FFTW_FORWARD flag used.
 * The function re-interprets the pointer
 * to the matrix as that of "fftw_complex" arrays.
 * @param[in] 2D Matrix<complex> input for fourier transform
 * @param[out] 2D Matrix<complex> output of fourier transform
 */

    Matrix<complex> m_out(m_in.size());
    fftw_plan m_plan;

    m_plan = fftw_plan_dft_2d(
                                 m_in.rows(),
                                 m_in.cols(),
                                 reinterpret_cast<fftw_complex*>(m_in.data()),
                                 reinterpret_cast<fftw_complex*>(m_out.data()),
                                 FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_execute(m_plan);
    fftw_destroy_plan(m_plan);
    return m_out;

}

/* ------------------------------------------------------ */

inline Matrix<complex> FFT::itransform(Matrix<complex>& m_in) {
/*!
 * @brief Wrapper to compute backward fourier transform 2D of
 * matrix<comple> numbers. Note the FFTW_BACKWARD flag used.
 * The function re-interprets the pointer
 * to the matrix as that of "fftw_complex" arrays.
 * @param[in] 2D Matrix<complex> input for inverse fourier transform
 * @param[out] 2D Matrix<complex> output of inverse fourier transform
 */

    Matrix<complex> m_out(m_in.size());
    fftw_plan m_plan;

    m_plan = fftw_plan_dft_2d(
                                m_in.rows(),
                                m_in.cols(),
                                reinterpret_cast<fftw_complex*>(m_in.data()),
                                reinterpret_cast<fftw_complex*>(m_out.data()),
                                FFTW_MEASURE, FFTW_ESTIMATE);
    fftw_execute(m_plan);
    fftw_destroy_plan(m_plan);
    return m_out /= (m_in.size() * m_in.size()); // Normalise the result

}

/* ------------------------------------------------------ */
inline Matrix<std::complex<int>> FFT::computeFrequencies(int size) {
/*!
 * @brief Wrapper to compute the wavenumbers for a grid.
 * @param[in] int col or row size of material matrix (sqaure matrix assumption)
 * @param[out] The function returns a complex matrix where the real part of an element
 * is 'qx' and the imaginary part is 'qy'
 */

    Matrix<std::complex<int>> m_res(size);

        for(int i = 0; i < size; i++){
            for(int j = 0; j < size; j++){
                // std::cout<< i <<": " << computeFreqHelper(size, i) << " | " << j<<" : " << computeFreqHelper(size, j) <<std::endl;
                m_res(i, j) = std::complex<int> (computeFreqHelper(size, i), computeFreqHelper(size, j));
            }
        }

    return m_res;
}


inline int FFT::computeFreqHelper(int size, int queryIndex) {
/*!
 * @brief A helper function to compute list of frequencies
 * in one dimension. Being used during computeFrequencies()
 */
    if(size % 2 == 0){
        int midVal = size / 2 -1;

        if(queryIndex <= midVal){
            return queryIndex;
        }
        else{
            return -midVal -(queryIndex - midVal);
        }
    }

    else{
        int midVal = (size -1)/2;
        if(queryIndex <= midVal){
            return queryIndex;
        }
        else{
            return -midVal -(queryIndex -midVal -1);
        }
    }

}

#endif  // FFT_HH
