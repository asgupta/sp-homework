import numpy as np
import argparse

parser = argparse.ArgumentParser()
parser.add_argument(
    '-xbound', nargs='+', type=float, default=[-1.0, 1.0],
    help="provide lower & upper bounds for x position of particles like -xbound -1.0 1.0"
)
parser.add_argument(
    '-ybound', nargs='+', type=float, default=[-1.0, 1.0],
    help="provide lower & upper bounds for y position of particles like -ybound -1.0 1.0"
)
parser.add_argument(
    '-gSize', type=int, default=10,
    help="provide the size of the grid as -gSize 5, which will make 5x5 grid"
)
parser.add_argument(
    '-fname', default="material_points.csv",
    help="provide filename in which data should be stored -fname abs.csv which"
)

args = parser.parse_args()

x_pos = np.linspace(args.xbound[0], args.xbound[1], args.gSize)
y_pos = np.linspace(args.ybound[0], args.ybound[1], args.gSize)

positions = []
print("Generating data")
for x in x_pos:
    for y in y_pos:
        positions += [np.asarray([x, y, 0.0])]
        
# particle data
positions = np.asarray(positions)
velocities = np.zeros((args.gSize**2, 3))
forces = velocities.copy()
masses = np.zeros((args.gSize**2, 1))
# material specific data 
temperatures = np.zeros((args.gSize**2, 1))


file_data = np.hstack((positions, velocities, forces, masses, temperatures))
print("Saving generated data")
np.savetxt(args.fname, file_data, delimiter=' ')
print("Saved data in {}".format(args.fname))
