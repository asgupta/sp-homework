import argparse
import numpy as np

# command line parsing
parser = argparse.ArgumentParser()
parser.add_argument(
    '-xbound', nargs='+', type=float, default=[-1.0, 1.0],
    help="provide lower & upper bounds for x position of particles like -xbound -1.0 1.0"
)
parser.add_argument(
    '-ybound', nargs='+', type=float, default=[-1.0, 1.0],
    help="provide lower & upper bounds for y position of particles like -ybound -1.0 1.0"
)
parser.add_argument(
    '-gSize', type=int, default=512,
    help="provide the size of the grid as -gSize 5, which will make 5x5 grid"
)
parser.add_argument(
    '-radius', type=float, default=0.5,
    help="provide the radius of radial heat source"
)
parser.add_argument(
    '-fname', default="radialHeatSource.csv",
    help="provide filename in which data should be stored -fname abs.csv which"
)

# radial function
def radialHeatSource(
        xbound,
        ybound,
        gridSize,
        radius,
        fname):
    x_pos = np.linspace(xbound[0], xbound[1], gridSize)
    y_pos = np.linspace(ybound[0], ybound[1], gridSize)
    out = []
    print("Generating data")
    for x in x_pos:
        for y in y_pos:
            r = x**2 + y**2
            if r < radius:
                # inside the radius
                out += [[x, y, 1]]
            else:
                # outside the radius
                out += [[x, y, 0]]
    out = np.asarray(out)
    print("Saving generated data")
    np.savetxt(fname, out, delimiter=',')
    print("Saved data in {}".format(fname))
    return

def main():
    args = parser.parse_args()
    print(args)
    radialHeatSource(
        xbound=args.xbound,
        ybound=args.ybound,
        gridSize=args.gSize,
        radius=args.radius,
        fname=args.fname,
    )
    return

if __name__ == "__main__":
    main()
