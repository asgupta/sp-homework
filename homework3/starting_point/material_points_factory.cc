#include "material_points_factory.hh"
#include "compute_temperature.hh"
#include "csv_reader.hh"
#include "csv_writer.hh"
#include "material_point.hh"
#include <cmath>
#include <iostream>


Real MaterialPointsFactory::rho = 1;
Real MaterialPointsFactory::hC = 1;
Real MaterialPointsFactory::hK = 1;
Real MaterialPointsFactory::dT = 1e-3;


std::function<Real(Real, Real)> MaterialPointsFactory::volHeatSrcFunc = [](Real x, Real y) { return 1; };

/* -------------------------------------------------------------------------- */
std::unique_ptr<Particle> MaterialPointsFactory::createParticle() {
/*!
 * @brief create and return unique pointer of MaterialPoint class object
 * @param[in] Void
 * @param[out] unique pointer of created object
 *
 */
  return std::make_unique<MaterialPoint>();
}

/* -------------------------------------------------------------------------- */

SystemEvolution&
MaterialPointsFactory::createSimulation(const std::string& fname,
                                        Real timestep) {
/*!
 * @brief create and return object  SystemEvolution class
 * @param[in] fname: name of csv file which stores the list of particles
 * @param[out] timestep: timestep for evolution of the system (dt)
 *
 */

  /// create SystemEvolution class's unique pointer
  /// by passing a unique pointer of System class object
  this->system_evolution =
      std::make_unique<SystemEvolution>(std::make_unique<System>());

  /// Read particles in CSV
  CsvReader reader(fname);
  reader.read(this->system_evolution->getSystem());
  /// Ensure no of particles is a square number
  auto N = this->system_evolution->getSystem().getNbParticles();
  int side = std::sqrt(N);
  /// Otherwise raise runtime error
  if (side * side != N)
    throw std::runtime_error("number of particles is not square");

  /// Make an shared pointer for ComputeTempreature class object.
  auto temperature = std::make_shared<ComputeTemperature>();
  /// Add it to systems computation lists
  /// - This is the list of compute class objects to be evaluated
  this->system_evolution->addCompute(temperature);

  return *system_evolution;
}

/* -------------------------------------------------------------------------- */

ParticlesFactoryInterface& MaterialPointsFactory::getInstance() {
/*!
 * @brief Create a unique instance of MaterialPointFactory type of ParticlesFactoryInterface
 * @param[in] Void
 * @param[out] MaterialPointsFactory object
 *
 */
  if (not ParticlesFactoryInterface::factory)
    ParticlesFactoryInterface::factory = new MaterialPointsFactory;

  return *factory;
}
/* -------------------------------------------------------------------------- */
