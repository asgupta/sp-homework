#ifndef __MATERIAL_POINTS_FACTORY__HH__
#define __MATERIAL_POINTS_FACTORY__HH__

/* -------------------------------------------------------------------------- */
#include "particles_factory_interface.hh"
#include "material_point.hh"
#include "my_types.hh"
#include <functional>
/* -------------------------------------------------------------------------- */

/*!
 * @class MaterialPointsFactory
 * @brief A class to help create simulation for particle of type
 * MaterialPoint class
 *
 * @note: dT timestep is added in the factory since there would be one
 * timestep that should be used throught the simulation, independent of
 * the kind of compute (temperature, etc) that needs to be performed
 *
 * @note:
 */
class MaterialPointsFactory : public ParticlesFactoryInterface {

private:
  MaterialPointsFactory() = default;

// protected:
public:
  /// mass density
  static Real rho;
  /// heat conductivity
  static Real hK;
  /// heat capacity
  static Real hC;
  /// time step
  static Real dT;

  static std::function<Real(Real, Real)> volHeatSrcFunc;
public:
  SystemEvolution& createSimulation(const std::string& fname,
				    Real timestep) override;

  std::unique_ptr<Particle> createParticle() override;

  static ParticlesFactoryInterface& getInstance();

  // Return reference of variables to allow
  // manipulation of their data
  static Real& getRho() { return rho; }
  static Real& gethK() { return hK; }
  static Real& gethC() { return hC; }
  static Real& getdT() { return dT; }
  /*!
   * @brief setVolHeatSrcFunc stores a functor of volumetric heat source functions
   * implemented by the user. This function is later used for ComputeTemperature
   * @note This function should be ideally moved in ComputeTemperature class for better
   * encapsulation
   * @param[in] std::function
   * @param[out] void
   */
  static void setVolHeatSrcFunc(std::function<Real(Real, Real)>& volHSfunction) {
    volHeatSrcFunc = volHSfunction;
  };
};


/* -------------------------------------------------------------------------- */
#endif  //__MATERIAL_POINTS_FACTORY__HH__
