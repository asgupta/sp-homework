#include "fft.hh"
#include "my_types.hh"
#include <gtest/gtest.h>

/*
 * Writing test case for inverse transform requires users to know
 * that output signal is scaled by the size of the input signal
*/

/*****************************************************************/
TEST(FFT, transform) {
  // check forward transform of FFT
  UInt N = 512;
  Matrix<complex> m(N);

  // generate signal array
  Real k = 2 * M_PI / N;
  for (auto&& entry : index(m)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    val = cos(k * i);
  }

  Matrix<complex> res = FFT::transform(m);

  for (auto&& entry : index(res)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    if (std::abs(val) > 1e-10)
      std::cout << i << "," << j << " = " << val << std::endl;

    if (i == 1 && j == 0)
      ASSERT_NEAR(std::abs(val), N * N / 2, 1e-10);
    else if (i == N - 1 && j == 0)
      ASSERT_NEAR(std::abs(val), N * N / 2, 1e-10);
    else
      ASSERT_NEAR(std::abs(val), 0, 1e-10);
  }
}
/*****************************************************************/

TEST(FFT, inverse_transform) {
  // check backward transform FFT
  UInt N = 512;
  Matrix<complex> m(N);

  // generate signal array
  Real k = 2 * M_PI / N;
  for (auto&& entry : index(m)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    val = cos(k * i);
  }

  // Stores the forward transform
  Matrix<complex> resF = FFT::transform(m);
  // Stores the backward transform
  Matrix<complex> resB = FFT::itransform(resF);

  // Comparing resB and m
  for (auto&& entry : index(resB)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    // the values are scaled by the size of the signal
    // after a inverse tranform, this does not happen in
    // python. Issue of FFTw3 lib as the signal is not
    // normalised before transform
    // ASSERT_NEAR(val.real()/(N * N), cos(k * i) , 1e-2);
    ASSERT_NEAR(val.real(), cos(k * i) , 1e-2);
  }
}

TEST(FFT, compute_freq) {
/* basic check to make sure that computeFrequencies output
 * matches depending on positive or negative size input
 */

    // f = [0, 1, ...,   n/2-1,     -n/2, ..., -1]   if n is even
    Matrix<std::complex<int>> resEven = FFT::computeFrequencies(10);
    ASSERT_EQ(resEven(4,0).real(), 4);
    ASSERT_EQ(resEven(5,0).real(), -5);
    ASSERT_EQ(resEven(0,4).imag(), 4);
    ASSERT_EQ(resEven(0,5).imag(), -5);

    // // f = [0, 1, ..., (n-1)/2, -(n-1)/2, ..., -1]   if n is odd
    Matrix<std::complex<int>> resOdd = FFT::computeFrequencies(11);
    ASSERT_EQ(resOdd(5,0).real(), 5);
    ASSERT_EQ(resOdd(6,0).real(), -5);
    ASSERT_EQ(resOdd(0,5).imag(), 5);
    ASSERT_EQ(resOdd(0,6).imag(), -5);


}
/*****************************************************************/
