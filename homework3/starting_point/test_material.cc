#include "csv_reader.hh"
#include "csv_writer.hh"
#include "material_point.hh"
#include "material_points_factory.hh"
#include "system.hh"
#include <gtest/gtest.h>
#include "compute_temperature.hh"

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>


/*****************************************************************/
// Fixture class
class MaterialGrid : public ::testing::Test {
protected:
    void SetUp(
        UInt grid_size,
        std::vector<Real> grid_lower_limits,
        std::vector<Real> grid_upper_limits ) {
        // get instance of MaterialPointsFactory class
        MaterialPointsFactory::getInstance();


        // Instead of using createSimulation which in turn
        // read on CSV files to add particles to the factory
        // we will add particles one by one
        std::vector<MaterialPoint> mParticleList;
        // grid_size = 3;
        Real dist_x = (grid_upper_limits[0] - grid_lower_limits[0])/(grid_size-1);
        Real dist_y = (grid_upper_limits[1] - grid_lower_limits[1])/(grid_size-1);
        for (UInt i = 0; i < grid_size; ++i) {
            for(UInt j = 0; j < grid_size; ++j){
                MaterialPoint parTemp;
                parTemp.getTemperature() = 1.;
                parTemp.getPosition()[0] = grid_lower_limits[0] + i*dist_x;
                parTemp.getPosition()[1] = grid_lower_limits[1] + j*dist_y;
                mParticleList.push_back(parTemp);
            }
        }

        for (auto& p : mParticleList) {
            // add particles to system as shared pointers
            system.addParticle(std::make_shared<MaterialPoint>(p));
        }
    }

    System system; // object of system for factory
    std::shared_ptr<ComputeTemperature> computeTemp;
    
};


/*****************************************************************/
TEST_F(MaterialGrid, test_Q2) {
    UInt grid_size = 3;
    std::vector<Real> grid_lower_limits {-1, -1};
    std::vector<Real> grid_upper_limits  { 1,  1};
    SetUp(grid_size, grid_lower_limits, grid_upper_limits);

    // Setting initial 
    MaterialPointsFactory::volHeatSrcFunc = [](Real x, Real y) { return 0; };

    computeTemp = std::make_shared<ComputeTemperature>();
    computeTemp->compute(system);

    double temp = static_cast<MaterialPoint&>(system.getParticle(4)).getTemperature();
    ASSERT_NEAR(temp, 1., 1e-2);
}

/*****************************************************************/
TEST_F(MaterialGrid, test_Q3) {
    UInt grid_size = 4;
    std::vector<Real> grid_lower_limits {-1, -1};
    std::vector<Real> grid_upper_limits  { 1,  1};
    SetUp(grid_size, grid_lower_limits, grid_upper_limits);

    // Setting initial 
    MaterialPointsFactory::volHeatSrcFunc = [](Real x, Real y) { return std::pow(2*M_PI/2,2)*std::sin(2*x*M_PI/2); };

    for(UInt pIndex = 0; pIndex < grid_size*grid_size; ++pIndex){
        auto& p = system.getParticle(pIndex);
        static_cast<MaterialPoint&>(p).getTemperature() = std::sin(2*M_PI*p.getPosition()[0]/2);
    }

    computeTemp = std::make_shared<ComputeTemperature>();
    computeTemp->compute(system);

    double temp1 = static_cast<MaterialPoint&>(system.getParticle(5)).getTemperature();
    double temp2 = static_cast<MaterialPoint&>(system.getParticle(6)).getTemperature();
    double temp3 = static_cast<MaterialPoint&>(system.getParticle(9)).getTemperature();
    double temp4 = static_cast<MaterialPoint&>(system.getParticle(10)).getTemperature();


    ASSERT_NEAR(temp1, -0.87, 1e-2);
    ASSERT_NEAR(temp2, -0.87, 1e-2);
    ASSERT_NEAR(temp3, 0.87, 1e-2);
    ASSERT_NEAR(temp4, 0.87, 1e-2);

}

TEST_F(MaterialGrid, test_Q4) {
    UInt grid_size = 5;
    std::vector<Real> grid_lower_limits {-1, -1};
    std::vector<Real> grid_upper_limits  { 1,  1};
    SetUp(grid_size, grid_lower_limits, grid_upper_limits);

    // Setting initial 
    MaterialPointsFactory::volHeatSrcFunc = [](Real x, Real y) { 
        if(x == 0.5){
            return 1;
        }
        else if (x == -0.5){
            return -1;
        }
        else{
            return 0;
        };
    };

    for(UInt pIndex = 0; pIndex < grid_size*grid_size; ++pIndex){
        auto& p = system.getParticle(pIndex);
        auto xPos = p.getPosition()[0];
        double eqTemp = 0;
        if(xPos <= -0.5){
            eqTemp = -xPos-1;
        }
        else if(xPos > 0.5){
            eqTemp = -xPos+1;
        }
        else{
            eqTemp = xPos;
        }
        static_cast<MaterialPoint&>(p).getTemperature() = eqTemp;
    }

    computeTemp = std::make_shared<ComputeTemperature>();
    computeTemp->compute(system);

    double temp1 = static_cast<MaterialPoint&>(system.getParticle(6)).getTemperature();
    double temp2 = static_cast<MaterialPoint&>(system.getParticle(7)).getTemperature();
    double temp3 = static_cast<MaterialPoint&>(system.getParticle(8)).getTemperature();
    double temp4 = static_cast<MaterialPoint&>(system.getParticle(18)).getTemperature();


    ASSERT_NEAR(temp1, -0.5, 1e-2);
    ASSERT_NEAR(temp2, -0.5, 1e-2);
    ASSERT_NEAR(temp3, -0.5, 1e-2);
    ASSERT_NEAR(temp4, 0.5, 1e-2);

}

TEST_F(MaterialGrid, test_Q5) {
    
    // Read the csv file
    // Assuming that the csv file must be generated with the default name for the test
    std::ifstream in("./../radialHeatSource.csv");
    std::vector<std::vector<double>> fields;

    if (in) {
        std::string line;

        while (getline(in, line)) {
            std::stringstream sep(line);
            std::string field;

            std::vector<double> dataEntry;
            while (getline(sep, field, ',')) {
                dataEntry.push_back(std::stod(field));
            }
            fields.push_back(dataEntry);
        }
    }
    
    UInt grid_size = std::sqrt(fields.size());
    std::vector<Real> grid_lower_limits {fields[0][0], fields[0][1]};
    std::vector<Real> grid_upper_limits {fields.back()[0], fields.back()[1]};
    SetUp(grid_size, grid_lower_limits, grid_upper_limits);

    
    // Setting initial 
    MaterialPointsFactory::volHeatSrcFunc = [&](Real x, Real y) { 
        
        int i = int((x - grid_lower_limits[0])*(grid_size-1)/(grid_upper_limits[0] - grid_lower_limits[0]));
        int j = int((y - grid_lower_limits[1])*(grid_size-1)/(grid_upper_limits[1] - grid_lower_limits[1]));
        
        return fields[j + grid_size*i][2];
    };

    // Currently assuming a uniform initial temperature distribution (temp = 1) for all particles (except at boundary)
    
    computeTemp = std::make_shared<ComputeTemperature>();
    computeTemp->compute(system);

    double temp1 = static_cast<MaterialPoint&>(system.getParticle(6)).getTemperature();
    double temp2 = static_cast<MaterialPoint&>(system.getParticle(7)).getTemperature();
    double temp3 = static_cast<MaterialPoint&>(system.getParticle(8)).getTemperature();
    double temp4 = static_cast<MaterialPoint&>(system.getParticle(18)).getTemperature();

    ASSERT_NEAR(temp1, 1, 1e-2);
    ASSERT_NEAR(temp2, 1, 1e-2);
    ASSERT_NEAR(temp3, 1, 1e-2);
    ASSERT_NEAR(temp4, 1, 1e-2);

}

