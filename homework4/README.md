# Homework 4 

## How to run the code
Git clone the repository & submodule init to get the submodules  
`$ cd path_to/homework4/starting_points` 
`$ mkdir build` 
`$ cd build` 
`$ cmake ..` 
`$ make` 
`$ mkdir dumps` 
`$ python3 main.py 10 2 ../circle_orbit.csv planet 0.1` 


## First Part: Eigen

`ComputeTemperatureFiniteDifferences` class uses eigne's `SparseLU` solver to find the next set of thetas at each timestep 
- We rewrite our equations in the form of Ax = b
- Where A is the `linearOperator` & b is the `rhsVector`

### Run tests for finite difference method 
`$ cd build/`
`$ ./test_eigen`

## Second Part: pybind11 

### Exercise 3:

- `createSimulation`: The function has the responsibility of
  - creating an object of `SystemEvolution` class using `unique_ptr`
  - reading the particles from the provided filename and populating the `system` inside `system_evolution`
  - Creating objects of desired daughter classes of `Compute`
  - Adding these objects to `system_evolution` for later processing during simulation


An overloaded implementation of `createSimulation` has been added
 - Allowing user to specify and add(to system) different daughter classes of `Compute`& their attributes 
 - Thus user does not need to touch the code inside `Factory` classes 
 - Also coupled with pybind it allows users to specify different desired `Compute` behaviour from python easily 
 
> Note: We have implemented createSimulation overloading in each of the daughter classes. 


### Exercise 4:

Reference to `Compute` type & its derived classes are using `shared_ptr` to ensure they are being correctly managed and are not deleted prematurely. 

### Test pybind

`$ cd build`
`$ mkdir dumps`
`$ python3 main.py 10 2 ../circle_orbit.csv planet 0.1`
