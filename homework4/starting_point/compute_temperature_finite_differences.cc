#include "compute_temperature_finite_differences.hh"
#include "material_point.hh"
#include "matrix.hh"
#include <Eigen/SparseLU>
#include <Eigen/OrderingMethods> 
#include <cmath>

void ComputeTemperatureFiniteDifferences::compute(System& system){

    assembleRightHandSide(system);

    assembleLinearOperator(system);
     
    Eigen::SparseLU<Eigen::SparseMatrix<Real> > solver;
    linearOperator.makeCompressed(); // A compressed
        
    solver.analyzePattern(linearOperator); 
    
    solver.factorize(linearOperator);
    
    auto next_theta = solver.solve(rhsVector); // b, Ax = b

    // do the LU factorization
    auto itr = next_theta.begin();
    for (auto& part : system) {
        auto& p = static_cast<MaterialPoint&>(part);
        p.getTemperatureRate() = (*itr) - p.getTemperature();
        p.getTemperature() = (*itr);
        ++itr;
    }
    
}

void ComputeTemperatureFiniteDifferences::assembleLinearOperator(
    System& system) {

    // Number of particles
    UInt size = system.getNbParticles();
    Real linOpConstant = this->delta_t * this->conductivity / (this->capacity * this->density);

    // Resize linearOperator
    this->linearOperator.resize(size, size);
    // this->linearOperator.reserve(5*size);
    this->linearOperator.reserve(Eigen::VectorXd::Constant(size,5));


    // Find dx or dy
    UInt N = std::sqrt(size); // Side length of the square grid
    Real a = this->L/(N-1);

    // Making vector of triplets for making sparse matrix
    for(UInt particleIndex = 0; particleIndex < size; ++particleIndex){

        // Index of current particle (column major)
        linearOperator.insert(particleIndex, particleIndex) = -4;

        // Refering to the 4 neighbouring particles 
        // For boundary cases, assumed that the grid wraps around like a toroid
        UInt north =  (particleIndex % N != 0) ? particleIndex-1 : particleIndex+N-1;
        UInt south = ((particleIndex+1) % N != 0) ? particleIndex+1 : particleIndex-(N-1);
        UInt east = (particleIndex > N-1) ? particleIndex-N : size - (N - particleIndex);
        UInt west = (particleIndex < N*(N-1)) ? particleIndex+N : N - (size - particleIndex);

        linearOperator.insert(particleIndex, north) = 1;
        linearOperator.insert(particleIndex, south) = 1;
        linearOperator.insert(particleIndex, east) = 1;
        linearOperator.insert(particleIndex, west) = 1;
            
    }

    // Divide by a^2
    linearOperator = linearOperator * linOpConstant / (a*a);
    
    Eigen::SparseMatrix<Real> sparseOnes(size, size);

    sparseOnes = Eigen::VectorXd::Constant(size, 1).asDiagonal();
    
    linearOperator = sparseOnes - linearOperator;
    
}

    void ComputeTemperatureFiniteDifferences::assembleRightHandSide(
    System& system) {
        
        // Number of particles
        UInt size = system.getNbParticles();
        Real rhsConstant = this->delta_t / (this->capacity * this->density);

        // Resize rhsVector
        this->rhsVector.resize(size);

        // Filling heat source values
        std::transform(system.begin(), system.end(), rhsVector.begin(),
            [rhsConstant](auto& part) {
                auto currentParticle = static_cast<MaterialPoint&>(part);
                return currentParticle.getTemperature() + currentParticle.getHeatSource() * rhsConstant;
            });
    }
