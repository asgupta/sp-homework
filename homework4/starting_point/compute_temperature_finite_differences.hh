#ifndef COMPUTE_TEMPERATURE_FINITE_DIFFERENCES_HH
#define COMPUTE_TEMPERATURE_FINITE_DIFFERENCES_HH

#include "compute.hh"
#include <Eigen/SparseCore>
#include <Eigen/SparseLU>

class ComputeTemperatureFiniteDifferences : public Compute {
public:
  // ComputeTemperatureFiniteDifferences(Real dt) {}

  void compute(System& system) override;
  void assembleLinearOperator(System& system);
  void assembleRightHandSide(System& system);

  //! return the heat conductivity
  Real& getConductivity() { return conductivity; };
  //! return the heat capacity
  Real& getCapacity() { return capacity; };
  //! return the heat capacity
  Real& getDensity() { return density; };
  //! return the characteristic length of the square
  Real& getL() { return L; };
  //! return the characteristic length of the square
  Real& getDeltat() { return delta_t; };





private:
  Real conductivity;
  Real capacity;
  Real density;
  //! side length of the problem
  Real L;
  Real delta_t;

  // Linear equation constants of
  // linearOperator x theta_(n+1) =  rhsVector
  Eigen::VectorXd rhsVector;
  Eigen::SparseMatrix<Real> linearOperator;

  



};

#endif  // COMPUTE_TEMPERATURE_FINITE_DIFFERENCES_HH
