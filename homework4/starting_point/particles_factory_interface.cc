#include "particles_factory_interface.hh"
#include "planets_factory.hh"
#include "csv_reader.hh"
/* -------------------------------------------------------------------------- */
ParticlesFactoryInterface& ParticlesFactoryInterface::getInstance() {
  return *factory;
}

/* -------------------------------------------------------------------------- */
ParticlesFactoryInterface* ParticlesFactoryInterface::factory = nullptr;

SystemEvolution& ParticlesFactoryInterface::createSimulation(
    const std::string& fname,
    Real timestep,
    const std::function<void(ParticlesFactoryInterface&, Real)>& createComputes) {

    this->system_evolution =
        std::make_unique<SystemEvolution>(std::make_unique<System>());

    CsvReader reader(fname);
    reader.read(this->system_evolution->getSystem());

    createComputes(getInstance(), timestep);

    return *system_evolution;
}
