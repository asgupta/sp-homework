#include <pybind11/pybind11.h>
#include <pybind11/functional.h>

namespace py = pybind11;

#include "compute_gravity.hh"
#include "compute_temperature.hh"
#include "compute_verlet_integration.hh"
#include "csv_writer.hh"
#include "material_points_factory.hh"
#include "ping_pong_balls_factory.hh"
#include "planets_factory.hh"
#include "particles_factory_interface.hh"
#include "compute.hh"
#include "compute_interaction.hh"
#include "system.hh"


PYBIND11_MODULE(pypart, m) {

  m.doc() = "pybind of the Particles project";
  // bind the routines here

  // ---------------- ParticlesFactoryInterface and its derived classes-------------------------
  // ParticlesFactoryInterface bindings
  // allow access to system_evolution of type unique_pointer
  auto system_evolution = [](const ParticlesFactoryInterface& p) {
                              return p.system_evolution.get();
                          };
  py::class_<ParticlesFactoryInterface>(
      m, "ParticlesFactoryInterface",
      py::dynamic_attr() // in case new attributes are needed 
      )
      .def_property_readonly("system_evolution",
                             system_evolution,
                             py::return_value_policy::reference_internal)
      .def("getInstance", &ParticlesFactoryInterface::getInstance,
          py::return_value_policy::reference)
      .def("createSimulation",
           py::overload_cast<
           const std::string&,
           Real,
           const std::function<void(ParticlesFactoryInterface&, Real)>&
           >(&ParticlesFactoryInterface::createSimulation),
           py::return_value_policy::reference);


  
  // MaterialPointsFactory bindings
  py::class_<MaterialPointsFactory, ParticlesFactoryInterface>(
      m, "MaterialPointsFactory", 
      py::dynamic_attr() // in case new attributes are needed 
      )
      .def_property_readonly("system_evolution",
                             system_evolution,
                             py::return_value_policy::reference_internal)
      .def("getInstance", &MaterialPointsFactory::getInstance,
          py::return_value_policy::reference)
      .def("createSimulation",
           py::overload_cast<const std::string&, Real>(&MaterialPointsFactory::createSimulation),
          py::return_value_policy::reference)
      .def("createSimulation",
           py::overload_cast<
           const std::string&,
           Real,
           const std::function<void(ParticlesFactoryInterface&, Real)>&
           >(&MaterialPointsFactory::createSimulation),
           py::return_value_policy::reference)
      .def("createParticle", &MaterialPointsFactory::createParticle,
           py::return_value_policy::reference);

  // PlanetsFactory bindings
  py::class_<PlanetsFactory, ParticlesFactoryInterface>(
      m, "PlanetsFactory", 
      py::dynamic_attr() // in case new attributes are needed 
      )
      .def_property_readonly("system_evolution",
                             system_evolution,
                             py::return_value_policy::reference_internal)
      .def("getInstance", &PlanetsFactory::getInstance,
          py::return_value_policy::reference)
      .def("createSimulation",
           py::overload_cast<const std::string&, Real>(&PlanetsFactory::createSimulation),
          py::return_value_policy::reference)
      .def("createSimulation",
           py::overload_cast<
           const std::string&,
           Real,
           const std::function<void(ParticlesFactoryInterface&, Real)>&
           >(&PlanetsFactory::createSimulation),
           py::return_value_policy::reference)
      .def("createParticle", &PlanetsFactory::createParticle,
           py::return_value_policy::reference);

  // PingPongBallsFactory bindings 
  py::class_<PingPongBallsFactory, ParticlesFactoryInterface>(
      m, "PingPongBallsFactory", 
      py::dynamic_attr() // in case new attributes are needed 
      )
      .def_property_readonly("system_evolution",
                             system_evolution,
                             py::return_value_policy::reference_internal)
      .def("getInstance", &PingPongBallsFactory::getInstance,
          py::return_value_policy::reference)
      .def("createSimulation",
           py::overload_cast<const std::string&, Real>(&PingPongBallsFactory::createSimulation),
           py::return_value_policy::reference)
      .def("createSimulation",
           py::overload_cast<
           const std::string&,
           Real,
           const std::function<void(ParticlesFactoryInterface&, Real)>&
           >(&PingPongBallsFactory::createSimulation),
           py::return_value_policy::reference)
      .def("createParticle", &PingPongBallsFactory::createParticle,
           py::return_value_policy::reference);

  // ----------------  Compute and its derived classes ---------------------------------------
  // Compute bindings
  py::class_<Compute, std::shared_ptr<Compute>>(
      m, "Compute",
      py::dynamic_attr() // in case new attributes are needed 
      );

  // ComputeInteraction bindings
  py::class_<ComputeInteraction, Compute, std::shared_ptr<ComputeInteraction>>(
      m, "ComputeInteraction", 
      py::dynamic_attr() // in case new attributes are needed 
      );

  // ComputeGravity bindings
  py::class_<ComputeGravity, ComputeInteraction, std::shared_ptr<ComputeGravity>>(
      m, "ComputeGravity", 
      py::dynamic_attr() // in case new attributes are needed 
      )
      .def(py::init<>())
      .def("compute", &ComputeGravity::compute)
      .def("setG", &ComputeGravity::setG);

  
  // Making provision to access and alter the private attributes associated with Compute objects
  auto conductivity = [](ComputeTemperature& t, Real _conductivity) {
                          t.getConductivity() = _conductivity;
                      };
  auto capacity = [](ComputeTemperature& t, Real _capacity) { t.getCapacity() = _capacity; };
  auto density = [](ComputeTemperature& t, Real _density) { t.getDensity() = _density; };
  auto L = [](ComputeTemperature& t, Real _L) { t.getL() = _L; };
  auto delta_t = [](ComputeTemperature& t, Real _delta_t) { t.getDeltat() = _delta_t; };

  // ComputeTemperature bindings
  py::class_<ComputeTemperature, Compute, std::shared_ptr<ComputeTemperature>>(
      m, "ComputeTemperature", 
      py::dynamic_attr() // in case new attributes are needed 
      )
      .def(py::init<>())
      .def("compute", &ComputeTemperature::compute)
      .def("conductivity", conductivity)
      .def("capacity", capacity)
      .def("density", density)
      .def("L", L)
      .def("delta_t", delta_t);

  // ComputeVerletIntegration bindings
  py::class_<ComputeVerletIntegration, Compute, std::shared_ptr<ComputeVerletIntegration>>(
      m, "ComputeVerletIntegration", 
      py::dynamic_attr() // in case new attributes are needed 
      )
      .def(py::init<Real>())
      .def("compute", &ComputeVerletIntegration::compute)
      .def("setDeltaT", &ComputeVerletIntegration::setDeltaT)
      .def("addInteraction", &ComputeVerletIntegration::addInteraction);

  // ----------------  Other classes for main.py ---------------------------------------
  // CsvWriter bindings
  py::class_<CsvWriter, Compute, std::shared_ptr<CsvWriter>>(
      m, "CsvWriter", 
      py::dynamic_attr() // in case new attributes are needed 
      )
      .def(py::init<const std::string &>())
      .def("write", &CsvWriter::write);  


  // SystemEvolution bindings
  py::class_<SystemEvolution>(
      m, "SystemEvolution", 
      py::dynamic_attr() // in case new attributes are needed 
      )
      .def("getSystem", &SystemEvolution::getSystem, py::return_value_policy::reference)
      .def("setNSteps", &SystemEvolution::setNSteps)
      .def("setDumpFreq", &SystemEvolution::setDumpFreq)
      .def("addCompute", &SystemEvolution::addCompute)
      .def("evolve", &SystemEvolution::evolve);

  // SystemEvolution bindings
  py::class_<System>(
      m, "System", 
      py::dynamic_attr() // in case new attributes are needed 
      );
}
