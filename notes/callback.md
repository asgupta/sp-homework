# Callbacks in optimization 


## Callback in [Linalg.Lgmres ](https://docs.scipy.org/doc/scipy/reference/generated/scipy.sparse.linalg.lgmres.html)

**callbackfunction, optional**

User-supplied function to call after each iteration. It is called as callback(xk), where xk is the current solution vector




## Callback in [Optimize.Minimize](https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.minimize.html)

**callbackcallable, optional**

Called after each iteration. For ‘trust-constr’ it is a callable with the signature:

callback(xk, OptimizeResult state) -> bool

where xk is the current parameter vector. and state is an OptimizeResult object, with the same fields as the ones from the return. If callback returns True the algorithm execution is terminated. For all the other methods, the signature is:

callback(xk)

- [Exampleas of using callbacks](https://www.py4u.net/discuss/17070)